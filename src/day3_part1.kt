import java.io.File

fun <T> List<T>.allCombinations(): List<Pair<T, T>> =
    flatMap { x -> map { y -> x to y } }

val fabric = Array(2000) { Array(2000) { 0 } }
// val squares = mutableListOf<OverlappingSquare>()

fun main() {
    val claims = File("real/day3_part1.txt").readLines().map(::Claim)

    claims.forEach { claim ->
        (claim.fromTop..(claim.fromTop + claim.height - 1)).forEach { row ->
            (claim.fromLeft..(claim.fromLeft + claim.width - 1)).forEach { col ->
                fabric[row][col] += 1
            }
        }
    }

    println(fabric.flatten().filter { it > 1 }.count())


    claims.filter { claim ->
        (claim.fromTop..(claim.fromTop + claim.height - 1)).flatMap { row ->
            (claim.fromLeft..(claim.fromLeft + claim.width - 1)).map { col ->
                fabric[row][col] == 1
            }
        }.all { it }
    }.let { println(it.first().id) }
//    fabric.forEachIndexed { rowIndex, row ->
//        row.forEachIndexed { cellIndex, cell ->
//            if (cell > 1) {
//                val result = squares.any { it.addIfContinuationOfSquare(rowIndex, cellIndex) }
//                if (!result)
//                    squares.add(OverlappingSquare(rowIndex to cellIndex))
//                println("$rowIndex, $cellIndex")
//            }
//        }
//
//        when (rowIndex) {
//            100 -> println("100")
//            500 -> println("At 500")
//            1000 -> println("Halfway done")
//            2000 -> println("Complete")
//        }
//
//    }

//    println(squares)
//    println(squares.map { it.squareInches }.sum())
}

//class OverlappingSquare(topLeftPoint: Pair<Int, Int>, private var width: Int = 1, private var height: Int = 1) {
//    private val top: Int = topLeftPoint.first
//    private val left: Int = topLeftPoint.second
//
//    private val bottom: Int get() = top + height
//    private val right: Int get() = left + width
//
//    val squareInches get() = width * height
//
//    fun addIfContinuationOfSquare(fromTop: Int, fromLeft: Int) = when {
//        left + width == fromLeft && fromTop in top.until(bottom) -> true.also { width += 1 }
//        top + height == fromTop  && fromLeft in left.until(right) -> true.also { height += 1 }
//        fromTop in top.until(bottom) && fromLeft in left.until(right) -> true
//        else -> false
//    }
//
//    override fun toString() = "$top,$left -> $width:$height"
//}

class Claim(private val s: String) {
    private val content = s.split(" ")
    private val inchesFrom = content[2].removeSuffix(":").split(",").map { it.toInt() }
    private val inchesSize = content[3].split("x").map { it.toInt() }

    val id = content[0].substring(1)
    val fromLeft = inchesFrom[0]
    val fromTop = inchesFrom[1]

    val width = inchesSize[0]
    val height = inchesSize[1]

    override fun toString() = "$s\n\t$id\n\t$fromLeft,$fromTop\n\t$width,$height"

}