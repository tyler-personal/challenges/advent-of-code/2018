fun main() {
    val numbers = generateSequence {
        val line = readLine()
        if (line == "done") null else line?.toIntOrNull()
    }
    println(numbers.sum())
}



