import java.io.File

fun main() {
    val ids = File("day2.txt").readLines()

    val twoOf = ids.getCountOfXOccurrences(2)
    val threeOf = ids.getCountOfXOccurrences(3)
    println(twoOf * threeOf)
}

fun List<String>.getCountOfXOccurrences(x: Int) = map { it.toCharArray() }
    .map { arr -> arr.groupBy { it }.filter { it.value.size == x } }
    .map { if (it.keys.count() >= 1) 1 else 0 }.sum()