import java.io.File
import java.math.BigInteger

fun main() {
    val numbers = File("fake.txt").readLines().map(String::toBigInteger)

    val list = mutableListOf<BigInteger>()


    var i = 0
    var result: BigInteger? = null
    while (result == null) {
        if (i >= numbers.size)
            i = 0
        result = getSumOnFirstDouble(numbers[i])
        i++
        Thread.sleep(500)
    }
    // println(trackedValues.filter { it == result })
    // println(result)
}

val trackedValues = mutableListOf<BigInteger>()

fun getSumOnFirstDouble(x: BigInteger): BigInteger? {
    val sum = trackedValues.fold(0.toBigInteger()) { acc, e -> acc + e } + x
    println(sum)

    return (if (sum in trackedValues) sum else null).also { trackedValues.add(sum) }
}