import java.io.File

fun main() {
    val ids = File("real/day2_part2.txt").readLines()
    val idsToCompare = ids.flatMap { x -> ids.map { y -> x to y } }

    idsToCompare.first { (x, y) ->
        0.until(x.length).filter { i -> x[i] != y[i] }.count() == 1
    }
        .let { (x, y) -> 0.until(x.length).filter { i -> x[i] == y[i] }.map { x[it] }.joinToString("") }
        .let(::println)
}