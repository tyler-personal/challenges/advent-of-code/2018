import java.io.File

fun main() {
    val numbers = File("input.txt").readLines().map { it.toLong() }
    var i = 0
    val uniqueSums = mutableSetOf(0L)
    var sum = 0L

    while (true) {
        if (i >= numbers.size)
            i = 0

        sum += numbers[i]

        if (sum !in uniqueSums)
            uniqueSums += sum
        else
            break
        i++
    }

    println(sum)
}
