import java.io.File
import Activity.*
import TimeComparison.*

fun main() {
    val entries = File("fake/day4.txt").readLines().map(::getEntry).sorted

    val guards = entries.fold(listOf<Guard>()) { acc, e ->
        when(e.id) {
            null -> acc.apply { last().entries.add(e) }
            in acc.map { it.id } -> acc.also { acc.find { it.id == e.id }!!.entries.add(e) }
            else -> acc + Guard(e.id, mutableListOf(e))

        }
    }
    guards.forEach(::println)
    val sleepyGuard = guards.maxBy { it.timeAsleep }!!
    val minuteMostAsleep = sleepyGuard.minuteMostAsleep
    sleepyGuard.nights.forEach { night ->
        println("New night:")
        night.entries.forEach { entry ->
            println(entry)
        }
        println()
    }
    println(minuteMostAsleep)
}

val List<Entry>.sorted get() = fold(listOf<Entry>()) { acc, e ->
    val a = acc[0]
    when {
        acc.isEmpty() -> listOf(e)
        acc.size == 1 && a.time.compare(e.time) == AFTER -> listOf(a, e)
        acc.size == 1 && a.time.compare(e.time) == BEFORE -> listOf(e, a)
        else -> acc.zipWithNext().flatMap { (a, b) -> when {
            e.time.compare(a.time) == AFTER && e.time.compare(b.time) == BEFORE -> listOf(a, e, b)
            else -> listOf(a, b)
        } }
    }
}

fun getEntry(data: String): Entry {
    val (left, content) = data.removePrefix("[").split("]").map { it.trim() }
    val (ymd, time) = left.split(" ")
    val (year, month, day) = ymd.split("-").map { it.toInt() }
    val (hours, minutes) = time.split(":").map { it.toInt() }
    val activity = when(content) {
        "wakes up" -> Activity.WAKES_UP
        "falls asleep" -> Activity.FALLS_ASLEEP
        else -> Activity.BEGINS_SHIFT
    }
    return Entry(Time(year, month, day, hours, minutes), content, activity)
}

data class Guard(val id: Int, val entries: MutableList<Entry>) {
    val nights: List<Night> get() = entries.fold(listOf()) { acc, e ->
        when(e.activity) {
            BEGINS_SHIFT -> acc + Night(this, mutableListOf(e))
            else -> acc.also { it.last().entries += e }
        }
    }
    val timeAsleep get() = nights.sumBy { it.timeAsleep }
    val minuteMostAsleep get() = nights.map { it.minuteMostAsleep }.groupBy { it }.maxBy { it.value.size }!!.key
}

data class Night(
    val guard: Guard,
    val entries: MutableList<Entry>
) {
    val sections = entries.zipWithNext().map { (a, b) ->

    }
    val timeAsleep get() = entries.zipWithNext().fold(0) { acc, e ->
        when (e.first.activity) {
            FALLS_ASLEEP -> acc + (e.second.minute - e.first.minute)
            else -> acc
        }
    }
    val minuteMostAsleep get() = entries.zip(entries.drop(1)).fold(mutableMapOf<Int, Int>()) { acc, e ->
        when(e.first.activity) {
            FALLS_ASLEEP -> acc.also { map -> e.first.minute.until(e.second.minute).forEach {
                map[it] = map[it]?.plus(it) ?: 1
            } }
            else -> acc
        }
    }.maxBy{ it.value }?.key ?: -1
}

data class Section(val startTime: Time, val endTime: Time)

data class Entry(val time: Time, val content: String, val activity: Activity) {
    val id = if (activity == BEGINS_SHIFT)
        content.split("#")[1].split(" ")[0].toInt()
    else null
}

class Time(val year: Int, val month: Int, val day: Int, val hour: Int, val minute: Int) {
    val compare: (o: Time) -> TimeComparison = when {
        o.year > year || o.month > month || o.day > day || o.hour > hour || o.minute > minute -> AFTER
        else -> BEFORE
    }
}

enum class TimeComparison {
    BEFORE, AFTER
}

enum class Activity {
    WAKES_UP, FALLS_ASLEEP, BEGINS_SHIFT
}

enum class GuardState {
    AWAKE, ASLEEP
}